wget https://github.com/nlohmann/json/archive/refs/tags/v3.11.2.tar.gz
tar -xf v3.11.2.tar.gz
mkdir json-3.11.2/build
cd json-3.11.2/build
cmake .. -DJSON_BuildTests=Off
make -j $JOBS
sudo make  install
cd -

wget https://github.com/actor-framework/actor-framework/archive/refs/tags/0.18.4.tar.gz
tar -xf 0.18.4.tar.gz
cd actor-framework-0.18.4
./configure
cd build
make -j $JOBS
sudo make install
cd ../..


wget https://github.com/fmtlib/fmt/archive/refs/tags/8.0.1.tar.gz
tar -xf 8.0.1.tar.gz
cd fmt-8.0.1/
mkdir build
cd build
cmake .. -DCMAKE_POSITION_INDEPENDENT_CODE=1 -DFMT_DOC=Off -DFMT_TEST=Off
make -j $JOBS
sudo make install
cd ../..

git clone https://github.com/AcademySoftwareFoundation/OpenTimelineIO.git
cd OpenTimelineIO
git checkout cxx17
mkdir build
cd build
cmake -DOTIO_PYTHON_INSTALL=ON -DOTIO_DEPENDENCIES_INSTALL=OFF -DOTIO_FIND_IMATH=ON ..
make -j $JOBS
sudo make install
cd ../..


