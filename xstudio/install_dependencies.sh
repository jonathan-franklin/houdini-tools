sudo dnf config-manager --set-enabled crb
sudo dnf update
sudo dnf groupinstall "Development Tools"
sudo dnf install git cmake python-devel pybind11-devel
sudo dnf install alsa-lib-devel pulseaudio-libs-devel
sudo dnf install freeglut-devel libjpeg-devel libuuid-devel
sudo dnf install doxygen python3-sphinx
sudo dnf install opus-devel libvpx-devel openjpeg2-devel lame-devel
sudo dnf install qt5 qt5-devel
pip install --user sphinx_rtd_theme breathe

