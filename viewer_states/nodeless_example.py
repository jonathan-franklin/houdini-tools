"""
State:          1 Nodeless Example
State type:     corundum::nodeless_example::1.0
Description:    Corundum::nodeless example::1.0
Author:         jonathan
Date Created:   December 01, 2023 - 13:56:34
"""


import hou
import viewerstate.utils as su

class State(object):
    def __init__(self, state_name, scene_viewer):
        self.state_name = state_name
        self.scene_viewer = scene_viewer


def createViewerStateTemplate():
    """ Mandatory entry point to create and return the viewer state
        template to register. """

    state_typename = "corundum::nodeless_example::1.0"
    state_label = "1 Nodeless Example"
    state_cat = hou.objNodeTypeCategory()

    template = hou.ViewerStateTemplate(state_typename, state_label, state_cat)
    template.bindFactory(State)
    template.bindIcon("MISC_python")






    return template

