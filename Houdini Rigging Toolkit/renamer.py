import hou as hou

def renamer(rename,padding):
    sel = hou.selectedNodes()  
    holding = rename.split('#')
    i = 1
    
    for node in sel:
        if padding == 1:
            newSel = holding[0] + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        elif padding == 2 and i < 10:
            newSel = holding[0] + "0" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        elif padding == 3 and i < 10:
            newSel = holding[0] + "00" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        elif padding == 3 and i < 100:
            newSel = holding[0] + "0" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        elif padding == 4 and i < 10:
            newSel = holding[0] + "000" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        elif padding == 4 and i < 100:
            newSel = holding[0] + "00" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        elif padding == 4 and i < 1000:
            newSel = holding[0] + "0" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
        else:
            newSel = holding[0] + "0" + str(i) + holding[1]
            hou.node("obj/" + str(node)).setName(newSel, 1)
            i += 1
