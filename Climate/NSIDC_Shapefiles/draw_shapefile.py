# Shapefile library ducumentation
# https://pypi.org/project/pyshp/

node = hou.pwd()
geo = node.geometry()

# Add code to modify contents of geo.
# Use drop down menu to select examples.

import os.path
import shapefile


root_dir = hou.evalParm("../../shapefile_directory")
filenum = str(node.evalParm("./pdg_index")).zfill(3)
filename = "median_extent_N_%s_1981-2010_polyline_v3.0.shp" % (filenum)
shape_path = os.path.join(root_dir, filename)

# Shapefile Data
sf = shapefile.Reader(shape_path)
shapes = sf.shapes()
points = shapes[0].points # This data set seems to only have one shape per file.
# List of the first point numbers of each independent segment
parts = shapes[0].parts
numpt = len(points)
numpart = len(parts)
next_part = 0

for index, part in enumerate(parts):
    if (index < numpart - 1):
        next_part = parts[index + 1]
    else:
        next_part = numpt - 1
    # Create new polyline per part
    line = geo.createPolygon(is_closed=False)
    part_points = points[part:next_part]
    for point in part_points:

        pos = (point[0], 0, point[1])
        point = geo.createPoint()
        point.setPosition(pos)
        line.addVertex(point)

