# Called when an in process work item needs to cook. In process work items
# are created by passing the `inProcess=True` flag when constructing the item in
# the `Generate` callback
#
# self              -   A reference to the current pdg.Node instance
# work_item         -   The work item being cooked by this callback

import urllib3
import certifi
import shutil
import glob
import hou

# Fetch $GEO
geo = hou.getenv('GEO')
http = urllib3.PoolManager(cert_reqs="CERT_REQUIRED", ca_certs=certifi.where())
base_path = geo + "/NSIDC/Sea_Ice_Index/shapefiles"

n = work_item.index
n = f'{n:03}' # Add numpadding
filename = 'median_extent_N_%s_1981-2010_polyline_v3.0.zip' % (n)
fileslice = filename[0:-4] # Remove .zip extension
file_path = os.path.join(base_path, filename)
shp_path = os.path.join(base_path, fileslice + ".shp")

if not os.path.isfile(shp_path):
    print("Downloading " + filename)
    # Download extent zip files
    with open(file_path, 'wb') as file:
        response = http.request('GET',
            'https://noaadata.apps.nsidc.org/NOAA/G02135/north/daily/shapefiles/dayofyear_median/median_extent_N_%s_1981-2010_polyline_v3.0.zip' % (n),
            preload_content=False)
        shutil.copyfileobj(response, file)
    # Extract extent zip files
    shutil.unpack_archive(file_path, base_path)

    for clean in glob.glob(os.path.join(base_path, fileslice) + "*"):
        # Delete all zip
        if clean.endswith(".zip"):
            os.remove(clean)

work_item.addOutputFile(shp_path)
